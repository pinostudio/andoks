<?php   session_start();
  $counter_name = "counter.txt";

  // Check if a text file exists.
  //If not create one and initialize it to zero.
  if (!file_exists($counter_name)) {
    $f = fopen($counter_name, "w");
    fwrite($f,"0");
    fclose($f);
  }

  // Read the current value of our counter file
  $f = fopen($counter_name,"r");
  global  $counterVal;

  $counterVal = fread($f, filesize($counter_name));
  fclose($f);

  // Has visitor been counted in this session?
  // If not, increase counter value by one
  if(!isset($_SESSION['hasVisited'])){
    $_SESSION['hasVisited']="yes";
    $counterVal++;
    $f = fopen($counter_name, "w");
    fwrite($f, $counterVal);
    fclose($f);
  }

  $chars = preg_split('//', $counterVal);


    

 $ip_name = "ip.txt";
        $ip = getenv('HTTP_CLIENT_IP')?:
          getenv('HTTP_X_FORWARDED_FOR')?:
          getenv('HTTP_X_FORWARDED')?:
          getenv('HTTP_FORWARDED_FOR')?:
          getenv('HTTP_FORWARDED')?:
          getenv('REMOTE_ADDR');
  if (!file_exists($ip_name)) {


          $ipfile = fopen($ip_name,"w");
          if (!isset($_SESSION['hasVisited'])):           
          fwrite($ipfile, $ip."\n" );
          fclose($ipfile);
          endif;
  }
  else  {

         $ipfile = fopen($ip_name, "a");
        if (!isset($_SESSION['hasVisited'])):         
          fwrite($ipfile, $ip."\n");
          fclose($ipfile);
           endif;

  }



  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123082970-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-123082970-1');
    </script>

  </head>
  <body <?php body_class(); ?>>

    <!-- Left float button -->
    <a class="btn btn-yellow left btn-locator" data-toggle="modal" data-target="#storeLocatorModal"><img src="<?php echo get_template_directory_uri().'/assets/imgs/marker.png' ?>" alt="Andoks"></a>
    
    <!-- Navigation -->
    <nav id="menu-2" class="offside">
      <a href="#" class="icon icon--cross menu-btn-2--close h--left">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      
      <div class="offside-logo">
        <img src="<?php echo get_template_directory_uri() ?>/assets/imgs/andoks-logo.png" class="offside-logo-img" alt="andoks-logo">
      </div>
      <div class="main-menu-wrapper">
        <div class="main-menu inline">
          <?php wp_nav_menu(array("menu" => 'Main Menu' )); ?>
        </div>
        
        
      </div>
    </nav>
    <!-- Site Overlay -->
    <div class="site-overlay"></div>
    <!-- Navigation -->
    <div id="offside-container">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container" id="nav-container-fluid">
          <div class="mobile-logo">
            <a class="nav-link js-scroll-trigger" href="<?php echo home_url(); ?>"><img src="<?php the_field('homepage_logo', 'option'); ?>" alt="Andoks"></a>
          </div>
          <a href="#" class="icon icon--hamburger menu-btn-2 h--right">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="collapse navbar-collapse text-center" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
              <?php /* Left navigation */
              wp_nav_menu( array(
              'menu' => 'Top Left Nav',
              'depth' => 2,
              'container' => false,
              'items_wrap' => '%3$s',
              )
              );
              ?>
            </ul>
            <div class="center-logo">
              <a class="nav-link js-scroll-trigger" href="<?php echo home_url(); ?>"><img src="<?php the_field('homepage_logo', 'option'); ?>" alt="Andoks"></a>
            </div>
            <ul class="navbar-nav ml-auto left-nav">
              <?php /* Right navigation */
              wp_nav_menu( array(
              'menu' => 'Top Right Nav',
              'depth' => 2,
              'container' => false,
              'items_wrap' => '%3$s',
              )
              );
              ?>
            </ul>
          </div>
        </div>
      </nav>