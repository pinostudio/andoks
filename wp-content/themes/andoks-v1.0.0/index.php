<?php get_header();
	$featured_image_1 = get_field('featured_image_1', 'options');
	$featured_title_1 = get_field('featured_title_1', 'options');
	$featured_description_1 = get_field('featured_description_1', 'options');
	$featured_link_1 = get_field('featured_link_1', 'options');
	$featured_image_2 = get_field('featured_image_2', 'options');
	$featured_title_2 = get_field('featured_title_2', 'options');
	$featured_description_2 = get_field('featured_description_2', 'options');
	$featured_link_2 = get_field('featured_link_2', 'options');
	$uploaded_media = get_field('media', 'options');
	/* Get the location */
	$args = array(
		'taxonomy' 	=> 'location-categories',
	);
	$locations = get_terms( $args );
?>

	<!-- REV SLIDER -->
	<div class="hero-slider">
		<?php echo do_shortcode('[rev_slider alias="andoks-slider"]') ?>
	</div>

	<!-- MEDIA SLIDER -->
	<?php if ($uploaded_media): ?>
		<div class="section primary-bg" id="media-slider">
			<div class="owl-carousel owl-theme" id="andoks-media-slider">
				<?php foreach ($uploaded_media as $media): ?>
						<?php if ($media['select_media'] == 'video'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" data-overlay="rgba(249,198,27,0.7)" data-vbtype="iframe" href="<?php echo $media['video'] ?>">
										<img src="<?php echo get_template_directory_uri() ?>/assets/imgs/placeholder-video.jpg" alt="Andoks">
										<svg class="video-overlay-play-button" viewBox="0 0 200 200">
											<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>
											<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>
											</svg>
									</a>
								</div>
							</div>
						<?php endif ?>
						<?php if ($media['select_media'] == 'url'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" data-autoplay="true" data-overlay="rgba(249,198,27,0.7)" data-vbtype="video" href="<?php echo $media['youtube_link'] ?>">
										<img src="<?php echo get_template_directory_uri() ?>/assets/imgs/placeholder-video.jpg" alt="Andoks">
										<svg class="video-overlay-play-button" viewBox="0 0 200 200">
											<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>
											<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>
											</svg>
									</a>
								</div>
							</div>
						<?php endif ?>
						<?php if ($media['select_media'] == 'image'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" href="<?php echo $media['image'] ?>"><img src="<?php echo $media['image'] ?>" /></a>
								</div>
							</div>
						<?php endif ?>
				<?php endforeach ?>
			</div>
		</div>
	<?php endif ?>

	<!-- FEATURED SECTION -->
	<div class="section" id="featured-items" style="background: url(<?php echo get_template_directory_uri().'/assets/imgs/wood-bg.jpg' ?>) center top;">
		<div class="container">
			<div class="row">
				<div class="col-md-6 featured-content">
					<div class="left-text text-center">
						<h3><?php echo $featured_title_1 ?></h3>
						<img class="img-responsive d-md-none" src="<?php echo $featured_image_1 ?>" alt="Andoks">
						<p><?php echo $featured_description_1 ?></p>
						<a href="<?php echo $featured_link_1 ?>" class="btn btn-red">View Menu</a>
					</div>
				</div>
				<div class="d-none d-md-block col-md-6 featured-content">
					<img class="right-img" src="<?php echo $featured_image_1 ?>" alt="Andoks">
				</div>
			</div>
			<div class="row">
				<div class="d-none d-md-block col-md-6">
					<div class="left-img-wrapper polaroid">
						<div style="background: url(<?php echo $featured_image_2;?>) center top; width: 350px; height: 370px;"></div>
						
					</div>
				</div>
				<div class="col-md-6">
					<div class="right-text">
						<h3><?php echo $featured_title_2 ?></h3>
						<div class="d-md-none left-img-wrapper polaroid">
						<div style="background: url(<?php echo $featured_image_2;?>) center top; width: 250px; height: 270px;"></div>
						
						</div>
						<p><?php echo $featured_description_2 ?></p>
						<a href="<?php echo $featured_link_2 ?>" class="btn btn-red">Locate Stores</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- STORE LOCATOR MODAL -->
	<div class="modal fade chicken-modal" id="storeLocatorModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog modal-lg vertical-align-center" role="document">
				<div class="modal-content">
					<img class="chicken-logo" src="<?php echo get_template_directory_uri().'/assets/imgs/andoks_chicken.png'; ?>" alt="Andoks Chicken">
					<div class="modal-header">
						<h2>Find Us Now</h2>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group search-bar">
							<img src="<?php echo get_template_directory_uri().'/assets/imgs/search-icon.png'?>" alt="icon-search">
							<input type="text" class="form-control" id="store-location" list="andoks-locations">
							<datalist id="andoks-locations">
								<?php foreach ($locations as $loc): ?>
										<option value="<?php echo $loc->name ?>" data-location="<?php echo $loc->term_id ?>">
								<?php endforeach ?>
							</datalist>
							
						</div>
						<a href="#" data-url="<?php echo home_url(); ?>" target="_blank" class="btn btn-red semi-rounded" id="btn-locator">Search</a>
					</div>
						
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>