<?php 
get_header(); 

$careers_bg = get_field('careers_background_image', 'option');
?>
<div id="careers" style="background:url(<?php echo $careers_bg; ?>) top/100% 1000px no-repeat;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="careers-category-tabs">
					<ul>
						<?php  
							$queried_object = get_queried_object();
							$term_id = $queried_object->term_id;
							$taxonomy = 'career_category';
							$terms = get_terms(
								$taxonomy,
								array(
									'parent'		=> 0,
									'hide_empty'	=> false,
								)
							);
							foreach( $terms as $term ) {
								if( $term_id == $term->term_id ) {
									$active = 'active';
								} else {
									$active = '';
								}
						?>
							<li>
								<a class="ubuntu-regular <?php echo $active; ?>" href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>

			
			<?php  
				$queried_object = get_queried_object();

				if( $queried_object->slug == 'recruitment-offices' ) {
					 view('careers/recruitment-offices');
				} else {
					view('careers/careers-accordion');
				}
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>