<?php  
	if(have_posts()):
		while(have_posts()): the_post();
?>
		<div class="container show-grid-5">
			<div class="row justify-content-center">
				<div class="col-lg-2 col-md-3 col-sm-12 no-pad-right c-pad-right">
					<div class="career-image-title-wrapper">
						<?php  
							if(has_post_thumbnail()):
								$career_image =  wp_get_attachment_url(get_post_thumbnail_id());
							else:
								$career_image = 'http://placehold.it/119x119';
							endif;
						?>
						<div class="career-image" style="background-image:url(<?php echo $career_image; ?>);"></div>

						<div class="career-title text-center">
							<h4 class="ubuntu-bold"><?php the_title(); ?></h4>
							<p class="ubuntu-bold">(<?php the_field('career_slot'); ?>)</p>
						</div>
					</div>
				</div>

				<div class="col-lg-8 col-md-9 col-sm-12 no-pad-left c-pad-left">
					<div class="careers-accordion">
						<?php
							$post_title = get_the_title();
							$accordion_title = strtolower( str_replace(' ', '-', $post_title) );

							$clean_accordion_title = clean($accordion_title);
						?>

						<div class="panel-group" id="accordion-<?php echo $clean_accordion_title; ?>" role="tablist" aria-multiselectable="true">
							<?php  
								if(have_rows('career_tabs_list')): $counter = 1;
									while(have_rows('career_tabs_list')): the_row('career_tabs_list'); 
							?>
									<?php
										$tab_title = get_sub_field('career_tab_title');
										$tab_content = get_sub_field('career_tab_content');

										if($counter == 1) {
											$active = 'show';
											$collapse = '';
										} else {
											$active = '';
											$collapse = 'collapsed';
										}													
									?>
									<div class="panel list-panel">
										<div class="list-heading" role="tab" id="<?php echo $clean_accordion_title; ?>-<?php echo $counter; ?>">
											<div class="list-title">
												<a class="<?php echo $collapse; ?> list-title-in ubuntu-medium" data-toggle="collapse" data-parent="#accordion-<?php echo $clean_accordion_title; ?>" href="#accordion-<?php echo $clean_accordion_title; ?>-<?php echo $counter; ?>" aria-expanded="true" >
													<?php echo $tab_title; ?>
												</a>
											</div>
										</div>

										<div id="accordion-<?php echo $clean_accordion_title; ?>-<?php echo $counter; ?>" class="panel-collapse collapse <?php echo $active; ?>" role="tabpanel" aria-labelledby="<?php echo $clean_accordion_title; ?>-<?php echo $counter; ?>">
											<div class="list-description">
												<?php echo $tab_content; ?>
											</div>
										</div>								
									</div>	
							<?php  
									$counter++;
									endwhile;
								endif; wp_reset_postdata();
							?>
						</div>	
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; 
		else:
	?>
	<div class="col-sm-12">
		<div class="no-careers-found text-center">
			<h2 class="ubuntu-bold">No Careers Found</h2>
		</div>
	</div>
<?php
	endif; wp_reset_postdata()
?>