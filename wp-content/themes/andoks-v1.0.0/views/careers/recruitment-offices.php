<div class="container">
	<div class="row justify-content-center">
		<div class="col-sm-12 ro-container-padding">
			<div class="we-need-you-title">
				<h2 class="ubuntu-regular">We Need You!</h2>
				<p class="ubuntu-regular">Interested candidates may visit our:</p>
			</div>


			<?php  
			$featured_main_office_args = array(
				'post_type'			=> 'careers',
				'posts_per_page'	=> 1,
				'ordeby'			=> 'date',
				'order'				=> 'DESC',
				'taxonomy'			=> 'career_category',
				'field' 			=> 'slug', 
				'term' 				=> 'recruitment-offices',
				'meta_query'		=> array(
					'relation'	=> 'OR',
					array(
						'key'		=> 'main_recruitment_office',
						'value'		=> '1',
						'compare'	=> '=',
					),
				)
			);
			$featured_main_office = new WP_Query($featured_main_office_args);
			if($featured_main_office): ?>
				<?php while($featured_main_office->have_posts()): $featured_main_office->the_post(); ?>
					<?php $post_not_in = get_the_id(); ?>
					<div class="col-lg-4 col-md-4 col-sm-12 ro-padding">
						<div class="ro-main-wrapper ro-wrapper-height bg-yellow">
							<h4 class="ubuntu-medium"><?php the_title(); ?></h4>
							<?php the_excerpt(); ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; wp_reset_postdata(); ?>
		</div>


		<div class="col-sm-12 ro-container-padding">
			<div class="onsite-recruitment-title">
				<p class="ubuntu-regular">On-site recruitment at:</p>
			</div>

			<div class="row c-padding">
				<?php 
				$recruitment_offices_args = array(
					'post_type'			=> 'careers',
					'posts_per_page'	=> -1,
					// 'ordeby'			=> 'date',
					// 'order'				=> 'DESC',
					'taxonomy'			=> 'career_category',
					'field' 			=> 'slug', 
					'term' 				=> 'recruitment-offices',
					'post__not_in'		=> array($post_not_in),
				);
				$recruitment_offices = new WP_Query($recruitment_offices_args); 
				if($recruitment_offices): ?>
					<?php while($recruitment_offices->have_posts()): $recruitment_offices->the_post(); ?>
						<div class="col-lg-4 col-md-4 col-sm-12 ro-padding">
							<div class="ro-main-wrapper ro-wrapper-height bg-white">
								<h4 class="ubuntu-medium"><?php the_title(); ?></h4>
								<?php the_excerpt(); ?>
							</div>
							<div class="clearfix show-grid"></div>
						</div>
					<?php endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>
			</div>

			<div class="recruitment-link text-center">
				<p class="ubuntu-regular">You may also send your resume to <a href="mailto:recruitment@andokscorp.com">recruitment@andokscorp.com</a></p>
			</div>	
		</div>
	</div>
</div>