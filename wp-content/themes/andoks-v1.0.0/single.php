<?php get_header(); ?>
				
<div class="single-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php if ( is_singular( 'b-locator' ) ): ?>
					<div class="title-part">
						<h2 class="opensans-bold">You will be redirected to locator page...</h2>
					</div>
					<input type="hidden" name="location-link" data-link="<?php echo get_post_type_archive_link( 'b-locator' ) ?>">
				<?php else: ?>
					<?php if ( have_posts() ): ?>
						<?php while( have_posts() ): the_post(); ?>
							<div class="title-part">
								<h2 class="opensans-bold"><?php the_title(); ?></h2>
								<p class="date opensans-bold"><?php the_date(); ?> <span class="opensans-light">by</span> <?php the_author(); ?></p>
							</div>
							<div class="content-part">
								<?php the_content(); ?>
							</div>
						<?php endwhile ?>
					<?php endif ?>
				<?php endif ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<?php get_footer();