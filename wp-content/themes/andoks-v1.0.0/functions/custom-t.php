<?php

//Get Map Long and Lat
function map_data($address){
	preg_match('/@([^"]+)/', $address, $match);
	$split = explode(',',$match[1]);
	$longlat = array();
	$longlat['latitude'] = $split[0];
	$longlat['longitude'] = $split[1];
	return $longlat;
}



/***remove special characters***/
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}



//Remove the type='*' attributes and values from wp_enqueue'ed scripts and styles
// add_filter('style_loader_tag', 'myplugin_remove_type_attr', 10, 2);
// add_filter('script_loader_tag', 'myplugin_remove_type_attr', 10, 2);

// function myplugin_remove_type_attr($tag, $handle) {
//     return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
// }