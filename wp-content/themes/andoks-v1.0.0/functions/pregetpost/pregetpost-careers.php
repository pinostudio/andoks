<?php
function careers_pregetposts( $query ) 
{
	if( is_admin() )
		return;
	if( ! is_admin() && $query->is_main_query() && is_tax( 'career_category' ) ) {
		$query->set( 'posts_per_page', -1 );
        $query->set( 'orderby', 'menu_order' );
        $query->set( 'order', 'ASC' );
	}

	return $query;
};
add_action('pre_get_posts', 'careers_pregetposts');