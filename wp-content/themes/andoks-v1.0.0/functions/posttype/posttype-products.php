<?php

$products_labels = array(
'name' => _x('Products', 'post type general name'),
'singular_name' => _x('Products', 'post type singular name'),
'add_new' => _x('Add New', 'Product'),
'add_new_item' => __('Add New Product'),
'edit_item' => __('Edit Product'),
'new_item' => __('New Products'),
'all_items' => __('All Products'),
'view_item' => __('View Product'),
'search_items' => __('Search Products'),
'not_found' =>  __('No Products Found'),
'not_found_in_trash' => __('No Products Found in Trash'), 
'parent_item_colon' => '',
'menu_name' => __('Products')

);

$products_args = array(
'labels' => $products_labels,
'public' => true,
'publicly_queryable' => true,
'show_ui' => true, 
'show_in_menu' => true, 
'query_var' => true,
'rewrite' => true,
'capability_type' => 'post',
'has_archive' => true, 
'hierarchical' => false,
'menu_position' => 5,
'supports' => array( 'title', 'editor', 'thumbnail', 'author','excerpt'),
'menu_icon'   => 'dashicons-carrot',
); 
register_post_type('products', $products_args);
