<?php
/*Change the wp-admin logo*/
function change_my_wp_login_image() {
	// Change #ffffff and #e1261c to customize you backend login page color
	echo "
	<style>
		.wp-core-ui {
			background-color: #f8bf00;
		}
		.wp-core-ui #login h1 a {
			background: url('".get_bloginfo('template_url')."/assets/imgs/home_logo.png') center/contain no-repeat;
			height:65px;
			width:100%; 
		}
		.wp-core-ui form {
		    background: #e1261c;
		}
		.wp-core-ui label {
			color: #fff;
		}
		.wp-core-ui input[type=text] {
			border: 1px solid #b1b1b1;
		}
		.wp-core-ui #login .button-primary.focus,
		.wp-core-ui #login .button-primary.hover, 
		.wp-core-ui #login .button-primary:focus, 
		.wp-core-ui #login .button-primary:hover {
		    background: #e1261c;
		    border-color: #ffffff;
		    -webkit-box-shadow: 0 0 0;
		    box-shadow: 0 0 0;
		    color: #ffffff;
		}
		.wp-core-ui #login .button-primary { 
			text-shadow: 0 0 0; 
			color: #e1261c;
			transition: all 0.3s;
		    -webkit-transition: all 0.3s;
		}

		.wp-core-ui #login .button-primary {
		    background: #ffffff;
		    border-color: #ffffff;
		    -webkit-box-shadow: inset 0 1px 0 #ffffff,0 1px 0 rgba(0,0,0,.15);
		    box-shadow: inset 0 1px 0 #ffffff,0 1px 0 rgba(0,0,0,.15);
    	}
    	.wp-core-ui #login input[type=text]:focus,
    	.wp-core-ui #login input[type=password]:focus {
			border-color: #ffffff;
			-webkit-box-shadow: 0 0 2px #ffffff;
			box-shadow: 0 0 2px #ffffff;
    	}
    	.wp-core-ui #login input[type=checkbox]:checked:before { color: #e1261c; }
    	.wp-core-ui #login input[type=checkbox]:focus {
			border-color: #ffffff;
			-webkit-box-shadow: 0 0 2px #ffffff;
			box-shadow: 0 0 2px #ffffff;
    	}
    	.login #backtoblog a:focus, .login #nav a:focus, .login h1 a:focus {
    		color:transparent !important;
    		-webkit-box-shadow:none !important;
    		box-shadow:none !important;
    	}
    	#tag-13 .delete { display: none; }
		.login #login_error, .login .message { border-left: 4px solid #ffffff; }

		.wp-core-ui .g-recaptcha {margin-bottom: 20px;}
	</style>
	";
}
add_action("login_head", "change_my_wp_login_image");

/*Change the wp-admin logo link*/
function loginpage_custom_link() {
	return site_url();
}
add_filter('login_headerurl','loginpage_custom_link');

/*Change the wp-admin logo title*/
function change_title_on_logo() {
	return site_url();
}
add_filter('login_headertitle', 'change_title_on_logo');

/*Hide update notification*/
function hide_update()
{
    remove_action( 'admin_notices', 'update_nag', 3 );
}
add_action( 'admin_head', 'hide_update', 1 );

/* Remove some stuff on backend */
function annointed_admin_bar_remove() {
	
    global $wp_admin_bar;

    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('updates');
    $wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

/**
 * Remove Rev Slider Metabox
 */
function remove_revolution_slider_meta_boxes() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type ) {
		remove_meta_box( 'mymetabox_revslider_0', $post_type, 'normal' );
	}
}
add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );

/**
 * Remove Dashboard Metabox
 */
function function_remove_ataglance() { 
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' ); 
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' ); 
	// remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
} 
add_action( 'wp_dashboard_setup', 'function_remove_ataglance' );	

// /*Hide update notification on footer*/
// add_filter( 'admin_footer_text', '__return_empty_string', 11 ); 
// add_filter( 'update_footer', '__return_empty_string', 11 );


// Admin Menu
function remove_admin_menus() {
	global $submenu;
	unset($submenu['themes.php'][6]); 
	remove_menu_page( 'edit.php' );  // Posts
	remove_menu_page( 'link-manager.php' ); // Links
	remove_menu_page( 'edit-comments.php' ); // Comments
	remove_menu_page( 'plugins.php' ); // Plugins
	//remove_menu_page( 'users.php' ); // Users
	remove_menu_page( 'tools.php' ); // Tools
	//remove_menu_page(‘options-general.php’); // Settings
	// remove_menu_page('wpcf7'); // Contact Form 7

	remove_submenu_page ( 'index.php', 'update-core.php' ); //Dashboard->Updates
	remove_submenu_page ( 'themes.php', 'themes.php' ); // Appearance-->Themes
	//remove_submenu_page ( 'themes.php', 'widgets.php' ); // Appearance-->Widgets
	remove_submenu_page ( 'themes.php', 'theme-editor.php' ); // Appearance-->Editor
	remove_submenu_page ( 'themes.php', 'customize.php' ); // Appearance-->Editor
	remove_submenu_page ( 'options-general.php', 'options-general.php' ); // Settings->General
	remove_submenu_page ( 'options-general.php', 'options-writing.php' ); // Settings->Writing
	remove_submenu_page ( 'options-general.php', 'options-reading.php' ); // Settings->Reading
	remove_submenu_page ( 'options-general.php', 'options-discussion.php' ); // Settings->Discussion
	remove_submenu_page ( 'options-general.php', 'options-media.php' ); // Settings->Media
	remove_submenu_page ( 'options-general.php', 'options-privacy.php' ); // Settings->Privacy

	remove_submenu_page ( 'plugins.php', 'plugin-editor.php' ); // Plugins->Editor

	remove_submenu_page( 'options-general.php', 'tinymce-advanced' );
	remove_submenu_page( 'options-general.php', 'swpsmtp_settings' );
	remove_submenu_page( 'options-general.php', 'radio-buttons-for-taxonomies' );
	remove_submenu_page( 'options-general.php', 'scporder-settings' );
	remove_submenu_page( 'options-general.php', 'breadcrumb-navxt' );

	remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action('admin_menu', 'remove_admin_menus', 102);