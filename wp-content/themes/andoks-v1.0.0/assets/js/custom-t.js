( function ($) {
	$.fn.extend({
		setequal: function( ){
		//var defaults = { findclass: '' };
		//var o = $.extend(defaults, options);

		//var c = o.findclass;
		// Get an array of all element heights
		var elementHeights = $(this).map(function() {
		return $(this).height();
		}).get();

		// Math.max takes a variable number of arguments
		// `apply` is equivalent to passing each height as an argument
		var maxHeight = Math.max.apply(null, elementHeights);

		$(this).height(maxHeight);

		return this;
		}
	});

	window.onload = function(){
		// Set each height to the max height
		$('#careers .ro-wrapper-height').setequal();
		$('#about_us .tab-wrapper').setequal();
	};


	$( document ).ready( function() {
		/**owl carousel about us gallery**/
		$("#owl-aboutus-gallery").owlCarousel({
			loop: false,
			nav: false,
			items: 1,
			dots: true,
			slideBy: 1,
			smartSpeed: 800,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        375:{
		        	items:1
		        },
		        414:{
		            items:1
		        },
		        768:{
		            items:1
		        },
		        992:{
		        	items:1
		        },
		        1000:{
		            items:1
		        }
		    }			
		});	
	} );


	$( window ).resize( function() {
	    $('#careers .ro-wrapper-height').css('height','auto');
	    $('#careers .ro-wrapper-height').setequal();


	    $('#about_us .tab-wrapper').css('height','auto');
	    $('#about_us .tab-wrapper').setequal();
	} );


	$( window ).load( function() {
  
	} );


	$( document ).scroll( function() {

	} );	
} ( jQuery ) );