(function($) {
    var offsideMenu2 = offside('#menu-2', {
        slidingElementsSelector: '#offside-container, #results',
        debug: true,
        buttonsSelector: '.menu-btn-2, .menu-btn-2--close',
        slidingSide: 'right',
        init: function() {},
        beforeClose: function() {}, // Function: Before close callback
        afterClose: function() {
            if ($('body').hasClass('offside-js--interact')) {
                $('body').removeClass('offside-js--interact');
            }
        },
    });
    var overlay = document.querySelector('.site-overlay')
        .addEventListener('click', window.offside.factory.closeOpenOffside);
    //Sticky header
    $(window).scroll(function() {
        if ($(document).scrollTop() > 100) {
            $('.fixed-top').addClass('shrink');
        } else {
            $('.fixed-top').removeClass('shrink');
        }
    });
    //Scroll to Top
    $(function() {
        $(document).on('scroll', function() {
            if ($(window).scrollTop() > 100) {
                $('.scroll-top-wrapper').addClass('show');
            } else {
                $('.scroll-top-wrapper').removeClass('show');
            }
        });
        $('.scroll-top-wrapper').on('click', scrollToTop);
    });

    function scrollToTop() {
        verticalOffset = typeof verticalOffset != 'undefined' ? verticalOffset : 0;
        element = $('body');
        offset = element.offset();
        offsetTop = offset.top;
        $('html, body').animate({
            scrollTop: offsetTop
        }, 500, 'linear');
    }
    $(window).resize(function() {
        if ($('.modal.in').length != 0) {
            setModalMaxHeight($('.modal.in'));
        }
    });
    $('#andoks-media-slider').owlCarousel({
        items: 1,
        nav: true,
        lazyLoad: true,
        center: true,
        navText: [
            "<img src='http://andoks.com.ph/wp-content/themes/andoks-v1.0.0/assets/imgs/arrow_left.png' alt='Andoks' />",
            "<img src='http://andoks.com.ph/wp-content/themes/andoks-v1.0.0/assets/imgs/arrow_right.png' alt='Andoks' />"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    $('.venobox').venobox({
        infinigall: true,
    });
    //Function to animate slider captions
    function doAnimations(elems) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        elems.each(function() {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function() {
                $this.removeClass($animationType);
            });
        });
    }
    //Variables on page load
    var $myCarousel = $('#andoks-hero-slider'),
        $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    //Initialize carousel
    $myCarousel.carousel();
    //Animate captions in first slide on page load
    doAnimations($firstAnimatingElems);
    //Pause carousel
    $myCarousel.carousel('pause');
    //Other slides to be animated on carousel slide event
    $myCarousel.on('slide.bs.carousel', function(e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });
    //Hide floating button in contact us page
    if ($('body').hasClass('home')) {
        $('.btn-locator').css('display', 'block');
        $('.modal').on('show.bs.modal', function() {
            $(this).show();
            // setModalMaxHeight(this);
        });
    }
    if (!$('body').hasClass('post-type-archive-b-locator')) {
        $(window).on('load', function() {
            $('#storeLocatorModal').modal('show');
        });
    }
    $('#store-location').on('input', function() {
        $('#btn-locator').removeAttr('href');
        var homeUrl = $('#btn-locator').data('url');
        var opt = $('option[value="' + $(this).val() + '"]');
        var locID = opt.length ? opt.data('location') : '';
        if (locID) {
            $('#btn-locator').attr('href', homeUrl + '/locator/?locationid=' + locID);
        }
    });



}(jQuery));