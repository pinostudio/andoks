<?php  
get_header();

$promos_bg = get_field('promos_background_image', 'option');
?>
<div id="promos" style="background:url(<?php echo $promos_bg; ?>) top/100% 100% no-repeat;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="promos-title">
					<h2 class="ubuntu-bold">Promos</h2>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="col-lg-8 col-md-8 col-sm-12 pro-padding">
				<?php  
				$main_promos = array(
					'post_type'			=> 'promos',
					'posts_per_page'	=> 1,
					'orderby'			=> 'date',
					'order'				=> 'DESC',
					'meta_query'		=> array(
						'relation'	=> 'OR',
						array(
							'key'		=> 'main_promo',
							'value'		=> '1',
							'compare'	=> '=',
						),
					)					
				);
				$main_promo = new WP_Query($main_promos);
				if($main_promo->have_posts()): ?>
					<?php while($main_promo->have_posts()): $main_promo->the_post(); ?>
						<div class="row">
							<div class="col-lg-7 col-md-7 col-sm-12 no-pad-right pad-right">
								<?php  
									$post_not_in = get_the_id();

									if(has_post_thumbnail()):
										$main_promo_img =  wp_get_attachment_url(get_post_thumbnail_id());
									else:
										$main_promo_img = 'http://placehold.it/458x257';
									endif;
								?>
								<div class="main-promo-image" style="background-image:url(<?php echo $main_promo_img; ?>);"></div>
							</div>

							<div class="col-lg-5 col-md-5 col-sm-12 no-pad-left pad-left">
								<div class="main-promo-wrapper">
									<div class="main-promo-title">
										<h4 class="ubuntu-bold"><?php the_title(); ?></h4>
									</div>

									<div class="main-promo-excerpt">
										<?php the_excerpt(); ?>
									</div>

									<div class="main-promo-rm-btn text-right">
										<a class="ubuntu-light" href="#">Read more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix show-grid"></div>
					<?php endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>
			</div>

			<?php  
			$promos = array(
				'post_type'			=> 'promos',
				'posts_per_page'	=> -1,
				'post__not_in'		=> array($post_not_in),
			);
			$promo = new WP_Query($promos);
			if($promo->have_posts()): ?>
				<?php while($promo->have_posts()): $promo->the_post(); ?>
					<div class="col-lg-4 col-md-4 col-sm-12 pro-padding">
						<?php  
							if(has_post_thumbnail()):
								$promo_img =  wp_get_attachment_url(get_post_thumbnail_id());
							else:
								$promo_img = 'http://placehold.it/371x257';
							endif;
						?>
						<div class="promo-image" style="background-image:url(<?php echo $promo_img; ?>);"></div>
						<div class="clearfix show-grid"></div>
					</div>
				<?php endwhile; ?>
			<?php endif; wp_reset_postdata() ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>