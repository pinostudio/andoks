<?php 

get_header(); 



$products_bg = get_field('products_background_image', 'option');

?>

<div id="products" style="background:url(<?php echo $products_bg; ?>) top/100% 100% no-repeat;">

	<div class="container">

		<div class="row justify-content-center">

			<?php  

				$taxonomy_name = 'product_category';

				$terms = get_terms( $taxonomy_name, array('parent'=>0, 'hide_empty'=>false,) );

				foreach( $terms as $term ) {

					$thumbnail = get_field('category_thumbnail', 'product_category_'.$term->term_id);	

					// First sub category
					//$term_children = get_term_children( $term->term_id, $taxonomy_name );
					$term_child = get_terms( $taxonomy_name, array('parent'=>$term->term_id, 'hide_empty'=>false) );

			?>

				<div class="col-lg-5 col-md-6 col-sm-12">

					<!-- <a href="<?php //echo get_category_link($term); ?>"> -->
					<a href="<?php echo get_term_link($term_child[0],$taxonomy_name); ?>">

						<div class="product-cat-image" style="background-image:url(<?php echo $thumbnail; ?>); background-color: #F8BF00; background-size: 150%;">
							<!-- <div class="white-spot"></div> -->
						</div>

					</a>

					<div class="product-cat-title text-center">

						<a class="ubuntu-bold " href="<?php echo get_term_link($term_child[0],$taxonomy_name); ?>"> <?php echo $term->name; ?> </a>

					</div>	

				</div>

			<?php } ?>

		</div>

	</div>

</div>

<?php get_footer();