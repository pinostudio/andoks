<?php get_header(); ?>
	<div id="locator">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5 nopadding list-location full-center">
					<?php if( have_posts() ): ?>
						<div class="content">
							<?php
								echo get_field( 'field_59e461da2b10e', 'option' ); // message
								echo do_shortcode('[b-locator]');
							?>
						</div>
					<?php endif ?>
				</div>
				<div class="col-lg-7 nopadding map-location">
					<?php echo do_shortcode('[b-locator-map]') ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

<?php
get_footer();