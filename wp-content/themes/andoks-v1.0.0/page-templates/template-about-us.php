<?php
/*
* Template Name: About Us Template
*/
get_header();
?>

<div id="about_us">
	<div class="about-us-bg" style="background:url(<?php the_field('about_us_background_image'); ?>) top/100% 100% no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm12">
					<div class="andoks-title">
						<!-- <h2 class="breeze regular"><span>A</span>ndok's</h2> -->
						<img src="<?php bloginfo('template_directory'); ?>/assets/imgs/andoks-about-image.png" class="img-fluid d-block mx-auto" alt="about-image">
					</div>
				</div>
			</div>
			<div class="clearfix"></div>

			<?php  
				if(have_rows('gallery_lists')): $counter=0;
					while(have_rows('gallery_lists')): the_row('gallery_lists'); $counter++;

					$gallery_image = get_sub_field('gallery_image');
					$gallery_desc = get_sub_field('gallery_description');
			?>
				<?php if($counter % 2 == 0) { ?>
					<div class="row justify-content-center">
						<div class="col-lg-5 col-md-7 col-sm-8">
							<div class="gallery_image full-center">
								<img src="<?php echo $gallery_image; ?>" class="img-fluid d-block mx-auto" alt="gallery-image">
							</div>
						</div>

						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="gallery_description">
								<?php echo $gallery_desc; ?>
							</div>
						</div>
					</div>
					<div class="clearfix __show_grid_70"></div>
				<?php } else { ?>
					<div class="row justify-content-center">
						<div class="col-lg-5 order-lg-2 col-md-7 col-sm-8">
							<div class="gallery_image full-center">
								<img src="<?php echo $gallery_image; ?>" class="img-fluid d-block mx-auto" alt="gallery-image">
							</div>
						</div>

						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="gallery_description">
								<?php echo $gallery_desc; ?>
							</div>
						</div>
					</div>
					<div class="clearfix __show_grid_70"></div>
				<?php } ?>
			<?php  
					endwhile;
				endif; wp_reset_postdata();
			?>
		</div>

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="tab-wrapper">
						<div class="tab-title">
							<h4 class="ubuntu-italic text-center"><?php the_field('vision_tab_title'); ?></h4>
						</div>

						<div class="tab-content">
							<?php the_field('vision_content'); ?>
						</div>
					</div>	
				</div>

				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="tab-wrapper">
						<div class="tab-title">
							<h4 class="ubuntu-italic text-center"><?php the_field('mission_tab_title'); ?></h4>
						</div>

						<div class="tab-content">
							<?php the_field('mission_content') ?>
						</div>
					</div>	
				</div>

				<div class="col-lg-5 col-md-4 col-sm-12">
					<div class="tab-wrapper">
						<div class="tab-title">
							<h4 class="ubuntu-italic text-center"><?php the_field('core_values_tab_title'); ?></h4>
						</div>

						<div class="tab-content">
							<?php the_field('core_values_content') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<?php get_footer(); ?>