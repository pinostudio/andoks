<?php
/*
* Template Name: About Us
*/
get_header();

$about_us_bg = get_field('gallery_background_image');
$about_content_bg = get_field('about_us_content_background');
?>

<div id="about_us">
	<div class="slider-gallery-bg" style="background:url(<?php echo $about_us_bg; ?>) top/100% 100% no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="about-us-title">
						<h2 class="ubuntu-bold"><?php the_title(); ?></h2>
					</div>

					<div class="slider-gallery-container">
						<div id="owl-aboutus-gallery" class="owl-theme">
							<?php  
								$gallery_images = get_field('about_us_gallery_images');
								if( $gallery_images ):
									foreach( $gallery_images as $gallery_image ):
										$url = $gallery_image['url'];
										$title = $gallery_image['title'];
							?>
								<div class="gallery-item">
									<img src="<?php echo $url; ?>" class="img-fluid mx-auto d-block" alt="<?php echo $title; ?>">
								</div>
							<?php  
									endforeach;
								endif; wp_reset_postdata()
							?>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="about-content-bg" style="background:url(<?php echo $about_content_bg; ?>) top/100% 100% no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="about-us-content">
						<?php  
							if(have_posts()):
								while(have_posts()): the_post();
									the_content();
								endwhile;
							endif; wp_reset_postdata()
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>	