<?php get_header(); ?>

<?php $products_bg = get_field('products_background_image', 'option'); ?>

<div id="products" style="background:url(<?php echo $products_bg; ?>) top/100% 100% no-repeat;">

	<div class="container">

		<div class="row">

			
			<div class="col-md-3">
			<?php
				// Get category id and name of selected category
				$category = get_queried_object();
				$parent_id = $category->term_id; 
				$parent_name = $category->name; 

				// Check if sub or parent category
				$current_cat_id = get_category($parent_id);
				if($current_cat_id->parent){
					// SUBCATEGORY
					$parent_id = $current_cat_id->category_parent;
					$parent_name = get_cat_name($parent_id);
					$sub_id = $current_cat_id->term_id;
				}else{
					//PARENT CATEGORY 
					
				}

				// Get category child of selected parent category
				// $term_id = $parent_id;
				// $taxonomy_name = 'product_category';
				// $term_children = get_term_children( $term_id, $taxonomy_name );
				// $term_children = get_terms( $taxonomy_name,array( 'child_of' => $term_id) );
				// var_dump($term_children);
				// echo '<div class="product-menu" style="background-color:#fff;">';
				// echo '<h3><strong>'.$parent_name.' Menu</strong></h3>';
				// echo '<ul>';
				// $term_children = get_categories(array('orderby' => 'slug'));
				// foreach ( $term_children as $child ) {
				// 	// $term = get_term_by( 'id', $child, $taxonomy_name );
				// 	$term = get_term_by( 'id', $child, $taxonomy_name );
				// 	$active_product = '';
				// 	if($sub_id==$term->term_id){
				// 		$active_product = "class='active'";
				// 	}
				// 	echo '<li '.$active_product.' ><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
				// }
				// echo '</ul>';
				// echo '<div class="product-back text-right"><a href="javascript:history.back()" ><strong><< Back</strong></a></div>';
				// echo '</div>';

	echo '<div class="product-menu" style="background-color:#fff;">';
	echo '<h3><strong>'.$parent_name.' Menu</strong></h3>';
	
$args = array(
       'hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $parent_id,
       'taxonomy' => 'product_category'
    );
  $subcats = get_categories($args);
    echo '<ul>';
      foreach ($subcats as $sc) {
     //  	$term = get_term_by( 'id', $sc, 'product_category' );
      	$active_product = '';
					if($sub_id==$sc->term_id){
						$active_product = "class='active'";
					}
        $link = get_term_link( $sc->slug, $sc->taxonomy );
          echo '<li '.$active_product.' ><a href="'. $link .'">'.$sc->name.'</a></li>';
      }
    echo '</ul>';
	echo '<div class="product-back text-right"><a href="javascript:history.back()" ><strong><< Back</strong></a></div>';
	echo '</div>';

?>
 
			</div>
			<div class="col-md-9">
				<?php if ( have_posts() ): ?>
					<?php while( have_posts() ): the_post(); ?>
						<?php $product_thumbnail=get_field('product-thumbnail'); ?>
						<div class="product-feature">
							<a class="venobox" href="<?php the_post_thumbnail_url();?>"><img src="<?php echo $product_thumbnail['url'] ?>" alt="<?php the_title(); ?>" style="width:100%"></a>
						</div>
						<!-- <h1><?php //the_post_thumbnail(); ?></h1> -->
					<?php endwhile ?>
				<?php endif ?>

			</div>

			<div class="clearfix"></div>
	

		</div>

	</div>

</div>



<?php get_footer();