
		<footer id="andoks-footer" style="background: url(<?php echo get_template_directory_uri().'/assets/imgs/footer_bg.jpg' ?>);background-size: cover;">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="social-media-menu">
							<?php /* Social Media navigation */
				                  wp_nav_menu( array(
				                      'menu' => 'Social Media Menu',
				                      'depth' => 0,
				                      'container' => false,
				                      'menu_class' => 'andoks-social-menu'
				                     )
				                  );
				              ?>
						</div>
						<div class="footer-nav">
							<?php /* Footer navigation */
				                  wp_nav_menu( array(
				                      'menu' => 'Footer Menu',
				                      'depth' => 0,
				                      'container' => false,
				                      'menu_class' => 'andoks-footer-menu'
				                     )
				                  );
				              ?>
						</div>
					</div>	
				</div>
			</div>
		</footer>
			  <div class="scroll-top-wrapper ">
				    <span class="scroll-top-inner">
				      <i class="fa fa-2x fa-arrow-circle-up"></i>
				    </span>
			  </div>
		</div>

		<?php wp_footer(); ?>
	</body>
</html>