<?php get_header(); ?>

<div class="page-not-found text-center">
	<div class="text-container">
		<h2>Sorry! This page doesn’t exist.</h2>
		<p>Go back to the <a href="<?php echo site_url(); ?>">homepage</a> for more flavor!</p>
	</div>
</div>

<?php get_footer();