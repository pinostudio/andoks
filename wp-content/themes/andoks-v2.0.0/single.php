<?php get_header(); ?>
				
<div class="single-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<!-- B LOCATOR -->
				<?php if ( is_singular( 'b-locator' ) ): ?>
					<div class="title-part">
						<h2 class="opensans-bold">You will be redirected to locator page...</h2>
					</div>
					<input type="hidden" name="location-link" data-link="<?php echo get_post_type_archive_link( 'b-locator' ) ?>">

				<!-- PRODUCT INNER PAGE -->
				<?php else: ?>
					<?php if ( have_posts() ): ?>
						<?php while( have_posts() ): the_post(); ?>
							<?php 
								$name = get_the_title();
								$description = get_field('description');
								$image = get_field('product-thumbnail');
							?>
							<div class="product-inner">
								<div class="row">
									<div class="col-lg-6">
										<div class="product-inner-text">
											<div class="andoks-breadcrumb">
												<?php 
													$product_cat = '';

													if (isset($_GET['key'])) {
														$product_cat = str_replace("-"," ",$_GET['key']);
													}
												?>

												<?php
													if ($product_cat=="dine in") {
														$url = get_site_url().'/product-category/favorites/';
													} else {
														$url = get_site_url().'/product-category/favorites-take-out/';
													}
												?>

												<a href="<?php echo $url; ?>">
													<span>Menu /</span>
												</a>
												<span><?php echo $product_cat; ?></span>
											</div>
											<h2>
												<?php echo $name; ?>
											</h2>
											<p>
												<?php echo $description; ?>
											</p>
										</div>										
									</div>
									<div class="col-lg-6">
										<div class="product-inner-image">
											<img src="<?php echo $image['url']; ?>" style="width: 100%;">
										</div>
									</div>
								</div>
							</div>
								
						<?php endwhile ?>
					<?php endif ?>
				<?php endif ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<?php get_footer();