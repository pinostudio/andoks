<?php 
get_header();

	$hero_image = get_field('career_image', 'option');

?>
<div id="careers">
	<div class="hero">
		<img src="<?php echo $hero_image; ?>">
		<div class="text-overlay">
			<h2>JOIN OUR GROWING TEAM!</h2>
		</div>
	</div>
	<div class="careers-container">
		<div class="careers-category-tabs">
			<ul>
				<?php  
					$queried_object = get_queried_object();
					$term_id = $queried_object->term_id;
					$taxonomy = 'career_category';
					$terms = get_terms(
						$taxonomy,
						array(
							'parent'		=> 0,
							'hide_empty'	=> false,
						)
					);
					foreach( $terms as $term ) {
						if( $term_id == $term->term_id ) {
							$active = 'active';
						} else {
							$active = '';
						}
				?>
				<?php
				if ($term->term_id !== 10): ?>
					<li class="<?php echo $active; ?>">
							<a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
					</li>
				<?php endif ?>
				<?php } ?>
			</ul>
			<div class="recruitment-office-banner">
				<div class="container-fluid">
					<div class="row">
						<div class="col col-xs-12 col-sm-12 col-md-9">
							<div class="rec-left-banner">
								<p>We need you! Send your resume to <a href="mailto:recruitment@andokscorp.com">recruitment@andokscorp.com</a> or visit our recruitment offices.</p>				
							</div>
						</div>
						<div class="col col-xs-12 col-sm-12 col-md-3 rec-right">
							<a href="<?php echo get_term_link($term); ?>">
								<div class="rec-right-banner">
									<span>
										<?php echo $term->name; ?>
									</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="career-cards">					
			<?php  
				$queried_object = get_queried_object();

				if( $queried_object->slug == 'recruitment-offices' ) {
					 view('careers/recruitment-offices');
				} else {
					view('careers/careers');
				}
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>