<?php

$careers_labels = array(
    'name' => _x('Careers', 'post type general name'),
    'singular_name' => _x('Careers', 'post type singular name'),
    'add_new' => _x('Add New', 'Careers'),
    'add_new_item' => __('Add New Career'),
    'edit_item' => __('Edit Career'),
    'new_item' => __('New Careers'),
    'all_items' => __('All Careers'),
    'view_item' => __('View Career'),
    'search_items' => __('Search Careers'),
    'not_found' =>  __('No Careers Found'),
    'not_found_in_trash' => __('No Careers Found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Careers')
);

$careers_args = array(
    'labels' => $careers_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array( 'title', 'thumbnail', 'author', 'excerpt'),
    'menu_icon'   => 'dashicons-businessman',
); 
register_post_type('careers', $careers_args);
