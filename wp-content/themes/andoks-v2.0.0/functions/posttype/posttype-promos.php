<?php

$promos_labels = array(
    'name' => _x('Promos', 'post type general name'),
    'singular_name' => _x('Promos', 'post type singular name'),
    'add_new' => _x('Add New', 'Promos'),
    'add_new_item' => __('Add New Promo'),
    'edit_item' => __('Edit Promo'),
    'new_item' => __('New Promos'),
    'all_items' => __('All Promos'),
    'view_item' => __('View Promo'),
    'search_items' => __('Search Promos'),
    'not_found' =>  __('No Promos Found'),
    'not_found_in_trash' => __('No Promos Found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Promos')
);

$promos_args = array(
    'labels' => $promos_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array( 'title', 'thumbnail', 'editor', 'author', 'excerpt'),
    'menu_icon'   => 'dashicons-awards',
); 
register_post_type('promos', $promos_args);
