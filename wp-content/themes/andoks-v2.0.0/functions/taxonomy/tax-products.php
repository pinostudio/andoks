<?php

$products_cat_labels = array(
    'name'                  => _x('Product Categories', 'taxonomy general name'),
    'singular_name'         => _x('Category', 'taxonomy singular name'),
    'add_new_item'          => __('Add New Category'),
    'new_item_name'         => __('New Category'),
    'edit_item'             => __('Edit Category'),
    'update_item'           => __('Update Category'),
    'all_items'             => __('All Categories'),
    'search_items'          => __('Search Categories'),
    'parent_item'           => __('Parent Category'),
    'parent_item_colon'     => __('Parent Categories:'),
    'menu_name'             => __('Product Categories'),
);
register_taxonomy('product_category',
    array('products'),
    array(
        'hierarchical'      => true,
        'labels'            => $products_cat_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'product-category'),
    )
);