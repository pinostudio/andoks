<?php
/* Change the default style font of tinymce */
if ( ! function_exists( 'my_theme_add_editor_styles' ) ) {
	function my_theme_add_editor_styles() 
	{
	    add_editor_style( 'assets/style/css/tinymce.css' );
	}
	add_action( 'after_setup_theme', 'my_theme_add_editor_styles' );
}

/* Customize mce editor font sizes */
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray )
	{
		$initArray['fontsize_formats'] = "12px 13px 14px 16px 18px 19px 20px 22px 24px 26px 28px 30px 32px 36px 40px 46px 72px 80px";
		return $initArray;
	}
	add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );
}

/* Add custom Fonts to the Fonts list */
if ( ! function_exists( 'wpex_mce_google_fonts_array' ) ) {
	function wpex_mce_google_fonts_array( $initArray )
	{
		$initArray['font_formats'] = 'Ubuntu=Ubuntu, sans-serif; BreezePersonalUse=Breeze Personal Use';
		return $initArray;
	}
	add_filter( 'tiny_mce_before_init', 'wpex_mce_google_fonts_array' );
}

function my_mce_before_init_insert_formats( $init_array ) 
{  
	/* Class name that defined in css file in your 'plugin_mce_css' function */
	$classes = array(
		'thin', 'light', 'regular', 'medium', 'semi-bold', 'bold', 'extra-bold', 'black',
		'ubuntu-light', 'ubuntu-regular', 'ubuntu-medium'
	);
	
	$style_formats = array();

	/* Store the data */
	foreach ( $classes as $class ) {
		$style_formats[] = array(
			'title'   => $class,
			'inline'  => 'span',
			'classes' => $class
		);
	}

	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array; 
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

// get your custom style
function plugin_mce_css( $mce_css ) 
{
  if ( !empty( $mce_css ) )
    $mce_css .= ',';
    $mce_css .= get_bloginfo('template_url').'/assets/style/css/tinymce.css';
    return $mce_css;

}
add_filter( 'mce_css', 'plugin_mce_css' );