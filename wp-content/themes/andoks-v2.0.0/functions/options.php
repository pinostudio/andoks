<?php
/**
 * Add acf option page in every sidebar menu in wordpress backend.
 * Note: You must have ACF first with Opition
 **/


	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'manage_options',
		'position' 		=> 2,
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Location Settings',
		'menu_title' 	=> 'Location Settings',
		'menu_slug' 	=> 'location-settings',
		'capability' 	=> 'manage_options',
		'parent_slug'  => 'edit.php?post_type=b-locator'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Careers Settings',
		'menu_title' 	=> 'Careers Settings',
		'menu_slug' 	=> 'careers-settings',
		'capability' 	=> 'manage_options',
		'parent_slug'  => 'edit.php?post_type=careers'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Promos Settings',
		'menu_title' 	=> 'Promos Settings',
		'menu_slug' 	=> 'promos-settings',
		'capability' 	=> 'manage_options',
		'parent_slug'  => 'edit.php?post_type=promos'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Products Settings',
		'menu_title' 	=> 'Products Settings',
		'menu_slug' 	=> 'products-settings',
		'capability' 	=> 'manage_options',
		'parent_slug'  => 'edit.php?post_type=products'
	));			