<?php
/**
 * Include default scripts and css
 * Include in wp_header()
 **/
if ( ! function_exists( 'enqueue_style_script' ) ) {

	function enqueue_style_script() 
	{
		/* 
		 * Register/Hook Fonts
		 */	
		wp_enqueue_style( 'font-ubuntu-css','//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' );

		/* 
		 * Register/Hook Styles
		 */	
		
		/* Third Party Styles */
		wp_enqueue_style('font_awesome', THEME_URL .'/node_modules/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('bootstrap_css', THEME_URL .'/node_modules/bootstrap/dist/css/bootstrap.min.css');
		wp_enqueue_style('sweetalert2_css', THEME_URL .'/node_modules/sweetalert2/dist/sweetalert2.min.css');
		wp_enqueue_style('animate_css', THEME_URL .'/node_modules/animate-css/animate.css');
		wp_enqueue_style('owl_css', THEME_URL .'/node_modules/owl_carousel/owl.carousel.css');
		wp_enqueue_style('owl_default_css', THEME_URL .'/node_modules/owl_carousel/owl.theme.default.min.css');
		wp_enqueue_style('venobox_css', THEME_URL .'/node_modules/venobox/venobox.css');
		wp_enqueue_style('offside_css', THEME_URL .'/node_modules/offside/offside.css');
		

		wp_enqueue_style('general_css', THEME_URL .'/assets/style/css/general.css');
		wp_enqueue_style('tinymce_css', THEME_URL .'/assets/style/css/tinymce.css');
		/* 
		 * Register/Hook Scripts
		 */
		
		/* Third Party Script */
		wp_register_script( 'popper_js', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'popper_js' );
		
		wp_register_script( 'bootstrap_js', THEME_URL  .'/node_modules/bootstrap/dist/js/bootstrap.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'bootstrap_js' );

		wp_register_script( 'nicescroll', THEME_URL  .'/node_modules/nicescroll/jquery.nicescroll.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'nicescroll' );

		wp_register_script( 'sweetalert2_js', THEME_URL  .'/node_modules/sweetalert2/dist/sweetalert2.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'sweetalert2_js' );

		wp_register_script( 'sweetalert2_support_js', '//cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'sweetalert2_support_js' );

		wp_register_script( 'owl_js', THEME_URL .'/node_modules/owl_carousel/owl.carousel.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'owl_js' );

		wp_register_script( 'venobox_js', THEME_URL  .'/node_modules/venobox/venobox.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'venobox_js' );

		wp_register_script( 'offside_js', THEME_URL  .'/node_modules/offside/offside.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'offside_js' );

		/* AJAX Script */
		// wp_register_script( 'function-name', THEME_URL . '/ajax/function-name/function-name.js', array( 'jquery' ), NULL, true );
		// wp_enqueue_script( 'function-name' );
		
		/* Develoer's Custom Script */
		// wp_register_script( 'custom-*', THEME_URL . '/assets/js/custom-*.js', array( 'jquery' ), NULL, true );
		// wp_enqueue_script( 'custom-*' );
		
		wp_register_script( 'custom-js', THEME_URL  .'/assets/js/custom.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'custom-js' );

		wp_register_script( 'custom-r', THEME_URL  .'/assets/js/custom-r.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'custom-r' );

		wp_register_script( 'custom-t', THEME_URL  .'/assets/js/custom-t.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'custom-t' );


		//google map function
		if(is_page('contact-us')) {		
			wp_enqueue_script( 'map-js', get_template_directory_uri().'/assets/js/map.js',array(),true);

			$mapdata = map_data( get_field('google_map_address') );
			$myMapArray = array(
				'map_lat'	=> $mapdata['latitude'],
				'map_long'	=> $mapdata['longitude'],
				'map_title'	=> get_bloginfo('name'),
				// 'map_icon'	=> THEME_URL.'/assets/imgs/map-pin.png'
			);
			wp_localize_script( "map-js", "mapData", $myMapArray );			
		}
						

		/**
		* Browser Compatibility
		*/
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		if( preg_match( '/trident/i', $u_agent ) ) {
			/**
			* Enqueue your style/script only @ IE
			*/
			wp_enqueue_style('ie_css', THEME_URL .'/assets/browser-compatibility/ie.css');
			
		} elseif( preg_match( '/firefox/i', $u_agent ) ) {
			/**
			* Enqueue your style/script only @ Mozilla Firefox
			*/
			wp_enqueue_style('mozilla_css', THEME_URL .'/assets/browser-compatibility/mozilla.css');

		} elseif( preg_match( '/mac/i', $u_agent ) ) {
			/**
			* Enqueue your style/script only @ Safari
			*/
		} elseif( preg_match( '/chrome/i', $u_agent ) ) {
            /**
             * Enqueue your style/script only @ Google Chrome
             */
		} elseif( preg_match( '/Opera/i',$u_agent ) || preg_match( '/OPR/i',$u_agent ) ) {
			/**
			* Enqueue your style/script only @ Opera
			*/
		}

	}
	add_action('wp_enqueue_scripts','enqueue_style_script');
}

if ( ! function_exists( 'enqueue_admin_style_script' ) ) {

	function enqueue_admin_style_script() 
	{
		/* 
		 * Register/Hook Scripts for backend
		 */
		wp_register_script( 'backend', THEME_URL .'/assets/js/backend.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'backend' );

		 wp_register_script( 'visitor-counter', THEME_URL . '/ajax/visitor-counter/counter.js', array( 'jquery' ), NULL, true );
         wp_enqueue_script( 'visitor-counter' );
	}

	add_action( 'admin_enqueue_scripts', 'enqueue_admin_style_script' );
}



//Second solution : two or more files.
//If you're using a child theme you could use:
// get_stylesheet_directory_uri() instead of get_template_directory_uri()
add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
function load_admin_styles() {
    wp_enqueue_style('custom-backend', THEME_URL .'/assets/style/css/custom-backend.css');
}  

