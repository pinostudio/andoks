<?php get_header(); ?>

<div id="career-inner">
		<?php  
			$queried_object = get_queried_object();
			$term_id = $queried_object->term_id;
			$taxonomy = 'career_category';
			$terms = get_terms(
				$taxonomy,
				array(
					'parent'		=> 0,
					'hide_empty'	=> false,
				)
			);
			foreach( $terms as $term ) {
				if( $term_id == $term->term_id ) {
					$active = 'active';
				} else {
					$active = '';
				}

		if ($term->term_id == 10): ?>
			<div class="recruitment-office-banner">
				<div class="container">
					<div class="row">
						<div class="col col-lg-9">
							<div class="rec-left-banner">
								<p>We need you! Send your resume to <a href="mailto:rectruitment@andokscorp.com">rectruitment@andokscorp.com</a> or visit our recruitment offices.</p>				
							</div>
						</div>
						<div class="col col-lg-3 rec-right">
							<a href="<?php echo get_term_link($term); ?>">
								<div class="rec-right-banner">
									<span>
										<?php echo $term->name; ?>
									</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>
		<?php } ?>

	<?php  
		if(have_posts()):
			while(have_posts()): the_post();
	?>
		<?php  
			if(have_rows('position')): $counter = 1;
				while(have_rows('position')): the_row('position'); 
		
				$position = get_the_title();
				$description = get_sub_field('full_desc');
				$salary = get_sub_field('salary');
				$experience = get_sub_field('experience');
				$address = get_sub_field('address');

			?>
			<div class="career-inner-details">
				<h1>
					<?php echo $position; ?>
				</h1>

				<div class="details">
					<div class="container">

						<?php if (!empty($salary)): ?>
							<div class="row">
								<div class="col col-lg-12">
									<div class="career-icon">
										<i class="far fa-money-bill-alt"></i>									
									</div>
									<?php echo $salary; ?>
								</div>
							</div>
						<?php endif ?>

						<?php if (!empty($experience)): ?>
							<div class="row">
								<div class="col col-lg-12">
									<div class="career-icon">
										<i class="fas fa-briefcase"></i>									
									</div>
									<?php echo $experience; ?>
								</div>
							</div>
						<?php endif ?>

						<?php if (!empty($address)): ?>
							<div class="row">
								<div class="col col-lg-12">
									<div class="career-icon">
										<i class="fas fa-map-marker-alt"></i>									
									</div>
									<?php echo $address; ?>
								</div>
							</div>
						<?php endif ?>

					</div>
				</div>
				<div class="description">
					<?php echo $description; ?>				
				</div>
			</div>
			<?php  
					$counter++;
					endwhile;
				endif; wp_reset_postdata();
			?>
		<?php endwhile; 
		endif; wp_reset_postdata()
	?>
</div>	

<?php get_footer();