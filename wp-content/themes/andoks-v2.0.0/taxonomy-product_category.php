<?php get_header(); ?>

<?php
	$hero_image = get_field('image', 'option');
	$hero_heading = get_field('heading', 'option');
	$hero_subheading = get_field('subheading', 'option');	
?>

<div class="product-category-page">
	<?php
		// Get category id and name of selected category
		$category = get_queried_object();
		$parent_id = $category->term_id; 
		$parent_name = $category->name; 

		// Check if sub or parent category
		$current_cat_id = get_category($parent_id);
		if($current_cat_id->parent){
			// SUBCATEGORY
			$parent_id = $current_cat_id->category_parent;
			$parent_name = get_cat_name($parent_id);
			$sub_id = $current_cat_id->term_id;
		}else{
			//PARENT CATEGORY 
			
		}
	?>

	<!-- HEADER -->
	<div class="menu-cat">
		<img src="<?php echo $hero_image; ?>">
		<div class="text-overlay">
			<div class="row">
				<div class="col-md-8">
					<h2>
						<?php echo $parent_name; ?>
					</h2>
				</div>
				<div class="col-md-4 btn-container">
					<?php 
						if ($parent_name == "Take-Out") {
					?>
						<a href="<?php get_site_url() ?>/product-category/favorites/">Dine In</a>
					<?php
						} else {
					?>
						<a href="<?php get_site_url() ?>/product-category/favorites-take-out/">Take Out</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="product-nav flex-container">
		<?php
		$args = array(
		       'hierarchical' => 1,
		       'show_option_none' => '',
		       'hide_empty' => 0,
		       'parent' => $parent_id,
		       'taxonomy' => 'product_category'
		    );

		  $subcats = get_categories($args);
		    echo '<ul>';
		      foreach ($subcats as $sc) {
		      	$active_product = '';
					if($sub_id==$sc->term_id){
						$active_product = "class='active'";
					}
		        $link = get_term_link( $sc->slug, $sc->taxonomy );
					echo '<li '.$active_product.' ><a href="'. $link .'">'.$sc->name.'</a></li>';
		      }
		    echo '</ul>';
			echo '</div>';
		?>	
	</div>			

	<div class="product-list">
		<div class="container">
			<div class="row">
				<?php if ( have_posts() ): ?>
					<?php while( have_posts() ): the_post(); ?>
						<?php
						$product_thumbnail=get_field('product-thumbnail');
						$product_name=get_the_title();
						$product_price=get_field('price');
						$product_category='';

						$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

						if (strpos($url,'take-out') !== false) {
						    $product_category='take-out';
						} else {
						    $product_category='dine-in';
						}

						?>
						<div class="col-md-6">
							<div class="product-container">
								<a href="<?php the_permalink();?>?key=<?php echo $product_category; ?>">
									<img src="<?php echo $product_thumbnail['url'] ?>" alt="<?php the_title(); ?>">
									<div class="product-text">
										<div class="container">
											<h2><?php echo $product_name; ?></h2>
										</div>
									</div>
								</a>
							</div>
						</div>
					<?php endwhile ?>
				<?php endif ?>
			</div>
		</div>

		<!-- Pagination -->
		<div class="pagination-container">
			<?php
			global $wp_query;

			$big = 999999999;

			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages,
				'prev_text' => __('« '),
				'next_text' => __(' »')
			) );
			?>
		</div>
	</div>

</div>

<?php get_footer();