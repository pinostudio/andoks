<?php
/*
* Template Name: Contact Us
*/
get_header();

// $contact_us_bg_image = get_field('contact_us_background_image');
?>

<div id="contact_us">
	<div class="container">
		<div class="row">
			<div class="map-container col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="contact-us-map">
					<div id="map-area"></div>
				</div>
			</div>


			<div class="col col-xl-7 col-lg-7 col-md-12 col-sm-12 col-xs-12">
				<div class="contact-us-right">
					<!-- <div class="contact-us-title">
						<h2 class="ubuntu-bold">Contact Us</h2>
					</div> -->

					<div class="location-details">
						<?php
							$address = get_field('location_details');
							$telephone = get_field('telephone');
							$email = get_field('email');
						?>
						
						<div class="container">
							<div class="row">
								<div class="col-lg-6 details">
									<div class="row">
										<div class="col-lg-2">
											<h3>A</h3>
										</div>
										<div class="col-lg-10">
											<p>
												<?php echo $address; ?>
											</p>
										</div>
									</div>
								</div>
								<div class="col-lg-6 details">
									<div class="row">
										<div class="col-lg-2">
											<h3>T</h3>
										</div>
										<div class="col-lg-10">
											<p>
												<?php echo $telephone; ?>
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2">
											<h3>E</h3>
										</div>
										<div class="col-lg-10">
											<p>
												<?php echo $email; ?>
			
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php  
						if(have_rows('contact_details')):
							while(have_rows('contact_details')): the_row('contact_details');
					?>
							<?php  
								$contact_icon = get_sub_field('contact_details_icon');
								$contact_info = get_sub_field('contact_details_content');
							?>
							<div class="contact-details">
								<span><img src="<?php echo $contact_icon; ?>" class="img-fluid" alt="contact-icon"></span>
								<p class="ubuntu-light"><?php echo $contact_info; ?></p>
							</div>
					<?php  
							endwhile;
						endif; wp_reset_postdata();
					?>
					<!-- <div class="inquiry-title">
						<h2 class="ubuntu-bold">Inquiry Form</h2>
					</div> -->

					<div class="inquiry-form">
						<?php  
							$inquiry_form = get_field('inquiry_form_shortcode');
							echo do_shortcode( $inquiry_form );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
