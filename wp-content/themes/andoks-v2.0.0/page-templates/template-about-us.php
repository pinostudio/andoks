<?php
/*
* Template Name: About Us Template
*/
get_header();

$header_image = get_field('about_us_background_image');

?>

<!-- ABOUT -->
<div class="about hero">
	<img src="<?php echo $header_image; ?>">
	<div class="text-overlay">
		<h2>ABOUT US</h2>
	</div>
</div>

<div id="about_us">
	<div class="about-us-bg">
		<div class="container">
			<div class="clearfix"></div>

			<?php  
				if(have_rows('gallery_lists')): $counter=0;
					while(have_rows('gallery_lists')): the_row('gallery_lists'); $counter++;

					$gallery_image = get_sub_field('gallery_image');
					$gallery_desc = get_sub_field('gallery_description');
					$year = get_sub_field('gallery_year');
					$heading = get_sub_field('gallery_heading');		
			?>
				<?php if($counter % 2 == 0) { ?>
					<div class="gallery-container row justify-content-center">
						<div class="col-lg-4 col-md-7 col-sm-8">

							<div class="gallery_image full-center">
								<img src="<?php echo $gallery_image; ?>" class="img-fluid d-block mx-auto" alt="gallery-image">
							</div>
						</div>

						<div class="col-lg-8 col-md-12 col-sm-12">
							<div class="gallery_header gallery_header_right">
								<i class="fas fa-circle"></i>
								<h3><?php echo $year; ?></h2>
								<h2><?php echo $heading; ?></h2>
							</div>
							<div class="gallery_description gallery_desc_right">
								<?php echo $gallery_desc; ?>
							</div>
						</div>
					</div>
					<div class="clearfix __show_grid_70"></div>
				<?php } else { ?>
					<div class="gallery-container row justify-content-center">
						<div class="col-lg-4 order-lg-4 col-md-7 col-sm-8">
							<div class="gallery_image full-center">
								<img src="<?php echo $gallery_image; ?>" class="img-fluid d-block mx-auto" alt="gallery-image">
							</div>
						</div>

						<div class="col-lg-8 col-md-12 col-sm-12">
							<div class="gallery_header gallery_header_left">
								<i class="fas fa-circle"></i>
								<h3><?php echo $year; ?></h2>
								<h2><?php echo $heading; ?></h2>
							</div>
							<div class="gallery_description gallery_desc_left">
								<?php echo $gallery_desc; ?>
							</div>
						</div>
					</div>
					<div class="clearfix __show_grid_70"></div>
				<?php } ?>
			<?php  
					endwhile;
				endif; wp_reset_postdata();
			?>
		</div>

		<div class="mvc">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-4 col-sm-12">
						<div class="tab-wrapper">
							<img src="<?php echo get_theme_file_uri() ?>/assets/imgs/heart.png">
							<h4 class=""><?php the_field('vision_tab_title'); ?></h4>
							<p><?php the_field('vision_content'); ?></p>
						</div>	
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="tab-wrapper">
							<img src="<?php echo get_theme_file_uri() ?>/assets/imgs/heart.png">
							<h4 class=""><?php the_field('mission_tab_title'); ?></h4>
							<p><?php the_field('mission_content'); ?></p>
						</div>	
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="tab-wrapper">
							<img src="<?php echo get_theme_file_uri() ?>/assets/imgs/heart.png">
							<h4 class=""><?php the_field('core_values_tab_title'); ?></h4>
							<p><?php the_field('core_values_content'); ?></p>
						</div>	
					</div>
					
				</div>
			</div>
		</div>
	</div>	
</div>
<?php get_footer(); ?>