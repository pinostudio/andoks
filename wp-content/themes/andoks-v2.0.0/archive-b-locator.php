<?php get_header(); ?>
	<div id="locator">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5 nopadding list-location full-center">
					<?php if( have_posts() ): ?>
						<div class="content">
							<p>
								Find an Andok's near you!
							</p>
							<?php
								echo do_shortcode('[b-locator]');
							?>
						</div>
					<?php endif ?>
				</div>
				<div class="col-lg-7 nopadding map-location">
					<?php echo do_shortcode('[b-locator-map]') ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

<?php
get_footer();