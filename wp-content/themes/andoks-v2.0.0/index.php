<?php get_header();
	$store_heading = get_field('store_heading', 'options');
	$store_body = get_field('store_body', 'options');
	$store_btn = get_field('store_btn', 'options');
	$store_link = get_field('store_link', 'options');
	$about_heading = get_field('about_heading', 'options');
	$about_body = get_field('about_body', 'options');
	$about_btn = get_field('about_btn', 'options');
	$about_link= get_field('about_link', 'options');
	$bg_img= get_field('bg_img', 'options');

	$uploaded_media = get_field('media', 'options');
	$video_preview = get_field('video_preview', 'options');
	/* Get the location */
	$args = array(
		'taxonomy' 	=> 'location-categories',
	);
	$locations = get_terms( $args );
?>

<div id="home">
	<!-- REV SLIDER -->
	<div class="hero-slider">
		<?php echo do_shortcode('[rev_slider alias="andoks-slider"]') ?>
	</div>

	<!-- FEATURED PRODUCTS -->
	<?php 
		// the query
		$featured = new WP_Query(array(
			'post_type'=>'products',
			'post_status'=>'publish',
			'posts_per_page'=>6,
			'product_category'=>'featured'
		));
	?>
		 
	<div class="featured-products">
		<div class="container">
			<div class="row">
				<?php if ( $featured->have_posts() ) : ?>
				    <!-- the loop -->
				    <?php while ( $featured->have_posts() ) : $featured->the_post(); ?>
				    	<?php $product_thumbnail = get_field('product-thumbnail'); ?>
						<div class="col-md-6 col-lg-2 featured-product-container">
							<a href="<?php the_permalink();?>?key=favorites">
								<div class="featured-product">
									<img src="<?php echo $product_thumbnail['url'] ?>">
								</div>
							</a>
							<span class="featured-product-name"><?php the_title(); ?></span>
						</div>


				    <?php endwhile; ?>
				    <!-- end of the loop -->		 
				    <?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="btn-container">
			<a href="<?php echo get_site_url() ?>/product-category/favorites-take-out/" class="btn btn-red">
				<span>VIEW MENU</span>
			</a>
		</div>
	</div>

	<!-- STORE LOCATOR -->
	<div class="store-locator">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<h2><?php echo $store_heading; ?></h2>
					<p class="">
						<?php echo $store_body; ?>
					</p>
				</div>
				<div class="col-lg-3 btn-right">
					<a href="<?php echo get_site_url() . $store_link ?>" class="btn btn-yellow">
						<span><?php echo $store_btn; ?></span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- ABOUT -->
	<div class="home-about">
		<img src="<?php echo $bg_img; ?>">
		<div class="text-overlay">
			<h2><?php echo $about_heading ?></h2>
			<p><?php echo $about_body; ?></p>
			<a href="<?php echo get_site_url() . $about_link ?>" class="btn btn-red">READ MORE</a>
		</div>
	</div>


	<!-- MEDIA SLIDER -->
	<?php if ($uploaded_media): ?>
		<div class="section primary-bg" id="media-slider">
			<div class="owl-carousel owl-theme" id="andoks-media-slider">
				<?php foreach ($uploaded_media as $media): ?>
						<?php if ($media['select_media'] == 'video'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" data-overlay="rgba(249,198,27,0.7)" data-vbtype="iframe" href="<?php echo $media['video'] ?>">
										<img src="<?php echo $video_preview; ?>" alt="Andoks">
										<svg class="video-overlay-play-button" viewBox="0 0 200 200">
											<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>
											<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>
										</svg>
									</a>
								</div>
							</div>
						<?php endif ?>

						<?php if ($media['select_media'] == 'url'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" data-autoplay="true" data-overlay="rgba(249,198,27,0.7)" data-vbtype="video" href="<?php echo $media['youtube_link'] ?>">
										<img src="<?php echo $video_preview; ?>" alt="Andoks">
										<svg class="video-overlay-play-button" viewBox="0 0 200 200">
											<circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>
											<polygon points="70, 55 70, 145 145, 100" fill="#fff"/>
											</svg>
									</a>
								</div>
							</div>
						<?php endif ?>

						<?php if ($media['select_media'] == 'image'): ?>
							<div class="item">
								<div class="video-wrapper">
									<a class="venobox" data-gall="andoksGall" href="<?php echo $media['image'] ?>"><img src="<?php echo $media['image'] ?>" /></a>
								</div>
							</div>
						<?php endif ?>
				<?php endforeach ?>
			</div>
		</div>
	<?php endif ?>

	<!-- STORE LOCATOR MODAL -->	
</div>

<?php get_footer(); ?>