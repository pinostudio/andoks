
		<footer id="andoks-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="footer-nav">
							<div class="container">
								<div class="row">
									<div class="footer-group col-lg-3">
										<h2>DINE WITH US</h2>
									  <?php /* Footer navigation */
						                  wp_nav_menu( array(
						                      'menu' => 'Dine With Us',
						                      'depth' => 0,
						                      'container' => false,
						                      'menu_class' => 'andoks-footer-menu'
						                     )
						                  );
						              ?>
									</div>
									<div class="footer-group col-lg-3">
										<h2>FIND US</h2>
						              <?php /* Footer navigation */
						                  wp_nav_menu( array(
						                      'menu' => 'Find Us',
						                      'depth' => 0,
						                      'container' => false,
						                      'menu_class' => 'andoks-footer-menu'
						                     )
						                  );
						              ?>
									</div>
									<div class="footer-group col-lg-3">
										<h2>WORK WITH US</h2>
						              <?php /* Footer navigation */
						                  wp_nav_menu( array(
						                      'menu' => 'Work With Us',
						                      'depth' => 0,
						                      'container' => false,
						                      'menu_class' => 'andoks-footer-menu'
						                     )
						                  );
						              ?>
									</div>
									<div class="footer-group col-lg-3">
										<h2>TERMS & PRIVACY</h2>
						              <?php /* Footer navigation */
						                  wp_nav_menu( array(
						                      'menu' => 'Terms',
						                      'depth' => 0,
						                      'container' => false,
						                      'menu_class' => 'andoks-footer-menu'
						                     )
						                  );
						              ?>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<div class="col-lg-4">
						<div class="social-media-menu">
							<h2>ANDOK'S</h2>
							<?php /* Social Media navigation */
				                  wp_nav_menu( array(
				                      'menu' => 'Social Media Menu',
				                      'depth' => 0,
				                      'container' => false,
				                      'menu_class' => 'andoks-social-menu'
				                     )
				                  );
				              ?>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<small>&copy; Andok's <?php echo date('Y') ?> - All Rights Reserved</small>
			</div>
		</footer>
			  <div class="scroll-top-wrapper ">
				    <span class="scroll-top-inner">
				      <i class="fa fa-2x fa-arrow-circle-up"></i>
				    </span>
			  </div>
		</div>
	</div>

		<?php wp_footer(); ?>
	</body>
</html>