( function( $ ) {

	 $('.venobox').venobox(); 

	if( $( 'body' ).hasClass( 'single-b-locator' ) ) {

		window.location.href = $( 'input[name="location-link"]' ).data( 'link' );

	}



	/* Nicescroll */

		var args = {

			cursorwidth: 4,

	        zindex: 9999999,

	        horizrailenabled: false,

	        cursorborderradius: 3,

	        background: "transparent",

		};



		args['cursorcolor'] = '#e1261c';

		args['cursorborder'] = '1px solid #e1261c';

		args['autohidemode'] = true;

		$( 'html, body' ).niceScroll( args );



		args['cursorcolor'] = '#ece819';

		args['cursorborder'] = '1px solid #ece819';

		args['autohidemode'] = false;

		$( '#b-location > ul' ).niceScroll( args );

	/* Nicescroll */



	/**

	 * Script On Document Ready

	 */

	autoHeight( '.archive-container .details' );



	/**

	 * Script On Resize

	 */

	$( window ).resize( function( $ ) {

	    autoHeight( '.archive-container .details' );

	} );



	/**

	 * Functions

	 */

	function autoHeight( resource ) {

		$( resource ).height( 'auto' );

		var maxHeight = Math.max.apply( null, $( resource ).map( function() {

		    return $( this ).height();

		} ).get() );



		$( resource ).height( maxHeight );

	}

} ( jQuery ) ); 

