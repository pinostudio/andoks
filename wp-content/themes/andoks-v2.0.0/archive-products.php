<?php 

get_header();

?>

<?php
	$hero_image = get_field('image', 'option');
	$hero_heading = get_field('heading', 'option');
	$hero_subheading = get_field('subheading', 'option');	
?>

<!-- ABOUT -->
<div class="products">
	<img src="<?php echo $hero_image; ?>">
	<div class="text-overlay">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<h2>
						<?php echo $hero_heading; ?>
					</h2>
					<p>
						<?php echo $hero_subheading; ?>
					</p>
				</div>
				<div class="col-lg-4 btn-container">
					<?php  

						$taxonomy_name = 'product_category';
						$terms = get_terms( $taxonomy_name, array('parent'=>0, 'hide_empty'=>false,) );

						foreach( $terms as $term ) {

							$thumbnail = get_field('category_thumbnail', 'product_category_'.$term->term_id);	

							// First sub category
							$term_child = get_terms( $taxonomy_name, array('parent'=>$term->term_id, 'hide_empty'=>false) );

					?>
						<a href="<?php echo get_term_link($term_child[0],$taxonomy_name); ?>" class="btn btn-red">
							<span><?php echo $term->name; ?></span>
						</a>

					<?php } ?>
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="products-wrapper">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ): ?>
				<?php while( have_posts() ): the_post(); ?>
					<?php
					$product_thumbnail=get_field('product-thumbnail');
					$product_name=get_the_title();
					$product_price=get_field('price');
					?>
					<div class="col-md-6 col-lg-4">
						<div class="product-container">
							<a href="<?php the_permalink();?>">
								<img src="<?php echo $product_thumbnail['url'] ?>" alt="<?php the_title(); ?>">
								<div class="product-text">
									<div class="container">
										<!-- <div class="row"> -->
											<!-- <div class="col-sm-8"> -->
												<h2><?php echo $product_name; ?></h2>
											<!-- </div> -->
											<!-- <div class="col-sm-4"> -->
												<!-- <php if (!empty($product_price)) { -->
													<!-- echo '<p>P'.$product_price.'</p>'; -->
												<!-- } > -->
											<!-- </div> -->
										<!-- </div> -->
									</div>
								</div>
							</a>
						</div>
					</div>
				<?php endwhile ?>
			<?php endif ?>
		</div>
	</div>

	<!-- Pagination -->
	<div class="pagination-container">
		<?php
		global $wp_query;

		$big = 999999999;

		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'prev_text' => __('« '),
			'next_text' => __(' »')
		) );
		?>
	</div>
</div>

<?php get_footer();
