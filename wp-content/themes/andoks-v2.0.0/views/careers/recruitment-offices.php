
<?php 
	$recruitment_offices_args = array(
		'post_type'			=> 'careers',
		'posts_per_page'	=> -1,
		// 'ordeby'			=> 'date',
		// 'order'				=> 'DESC',
		'taxonomy'			=> 'career_category',
		'field' 			=> 'slug', 
		'term' 				=> 'recruitment-offices',
		'post__not_in'		=> array($post_not_in),
	);

	$recruitment_offices = new WP_Query($recruitment_offices_args); 
?>



<div class="container">
	<div class="row">
		<?php
		if($recruitment_offices): ?>				
			<?php while($recruitment_offices->have_posts()): $recruitment_offices->the_post(); ?>
			<div class="col-lg-6">
				<div class="career-card">
					<div class="row">
						<div class="col-sm-1">
							<div class="career-tab"></div>	
						</div>
						<div class="col-sm-11">
							<div class="career-description">
								<div>
									<h2>
										<?php the_title(); ?>
									</h2>
									<div class="office-description">
										<?php while(the_repeater_field('office')): ?>
									        <?php echo get_sub_field('description'); ?>
									    <?php endwhile; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>
	</div>
</div>	