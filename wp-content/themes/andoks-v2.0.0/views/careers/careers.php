<div class="container">
	<div class="row">
	<?php  
		if(have_posts()):
			while(have_posts()): the_post();
	?>
			<?php  
				if(have_rows('position')): $counter = 1;
					while(have_rows('position')): the_row('position'); 
			
					$position = get_the_title();
					$shortdesc = get_sub_field('short_desc');											
				?>
			<div class="col-lg-6">
				<a href="<?php the_permalink(); ?>">
					<div class="career-card">
						<div class="row">
							<div class="col-sm-1 hide-on-small">
								<div class="career-tab"></div>	
							</div>
							<div class="col-sm-11">
								<div class="career-description">
									<div>
										<h2>
											<?php echo $position; ?>
										</h2>
										<p>
											<?php echo $shortdesc; ?>								
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>	
			<?php  
					$counter++;
					endwhile;
				endif; wp_reset_postdata();
			?>
		<?php endwhile; 
			else:
		?>
		<div class="col-sm-12">
			<div class="no-careers-found text-center">
				<h2>No Careers Found</h2>
			</div>
		</div>
	<?php
		endif; wp_reset_postdata()
	?>
	</div>
</div>	