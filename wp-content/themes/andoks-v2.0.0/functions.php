<?php
include_once( 'functions/initialization.php' );

include_once( 'functions/enqueue.php' );
include_once( 'functions/include-assets.php' );
include_once( 'functions/image-size.php' );
include_once( 'functions/views.php' );
include_once( 'functions/theme-support.php' );
include_once( 'functions/menus.php' );
include_once( 'functions/body-class.php' );
include_once( 'functions/wp-title.php' );
// include_once( 'functions/rewrite-rules.php' );
include_once( 'functions/options.php' ); /* Required ACF Plugin */
include_once( 'functions/tinymce.php' ); /* Reqyured Advanced TinyMCE Plugin */
include_once( 'functions/backend.php' );
// include_once( 'functions/page-cat.php' );
// include_once( 'functions/term-settings.php' );

/* Include all the file inside the specify subfolder of functions folder */
include_all_php('posttype');
include_all_php('pregetpost');
include_all_php('shortcode');
include_all_php('taxonomy');

/* AJAX Script */
include_once( 'ajax/visitor-counter/counter.php' );

/* Develoer's Custom Script */
// include_once( 'functions/custom-*.php' );
include_once( 'functions/custom-r.php' );
include_once( 'functions/custom-t.php' );




add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
  
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('custom_help_widget', 'Visitor Counter', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
  
  $counter_name =  "/home/andoksc1/public_html/counter.txt";
  $counterVal = file_get_contents($counter_name);
  // $counterVal = str_pad($counterVal, 5, "0", STR_PAD_LEFT);


  // echo $counterVal;
	$str = $counterVal;

	$price_text = (string)$str; // convert into a string
	$arr1 = str_split($price_text, $str);
	$price_new_text = implode(",", $arr1);

	$arr2 = str_split($price_new_text);

	// print_r($arr2)


 ?>


<div class="container-fluid __counter_container">
    <div class="row">
        <div class="__img_container" style="background: #d72105;">
            <img src="https://andoks.com.ph/wp-content/uploads/2017/12/new_logo.png" alt="Image" style="margin: 0 auto;display: block; height: 90px; ">
        </div>

        <div style="background: #f8ec00">
            <h1 class="__title_visitor">Visitor's Counter</h1>
			
			<div class="__count">            

            <?php  
            foreach ($arr2 as &$value) { ?>
				
				<div class="__number_bg">
                    <p class="text-center"><?php echo $value ?></p>
				</div>	
				
            <?php  }   ?>
 
        	</div>
        </div>

    </div>
</div>


<?php 

}

