( function( $ ) {
	var category, locations_data = {}, map_settings;

	get_categories();
	if ( $( '#b-location-list' ).hasClass( 'have-categories' ) ) {
		$( '#b-categories' ).on( 'change', function() {
			category = $( this ).val();
			get_categories();
		} );
	}

	function get_categories()
	{
		$( '.b-preloader' ).fadeIn();
		$.ajax( {
		    url      : ajaxurl,
		    type     : 'post',
		    dataType : 'json',
		    data     : {
		        action   : 'get_categories',
		        category : category
		    },
		    success  : function( response ) {
		    	$( '.b-preloader' ).fadeOut();

		    	/**
		    	 * Initialize the map and place all the location
		    	 */
		    	map_settings = response.map_settings

		    	var locations_result = '';

		    	locations_data = {};

		    	if ( response.filtered_locations ) {
		    		/**
		    		 * Get the categories data
		    		 */
					$.each( response.filtered_locations.categories, function( index, val ) {
		    			locations_result += '<option value="'+ val.category_name +'" data-id="'+ val.category_id +'"></option>';
		    		} );		    		

		    		/**
		    		 * Get the locations data
		    		 */
		    		$.each( response.filtered_locations.locations, function( index, val ) {
		    			/* Store the location data in the array that can be used in initializing map */
		    			locations_data[ val.location_id ] = [
							val.location_long,
							val.location_lat,
							val.location_details,
						];
		    		} );
		    		
		    		/**
		    		 * Initialize the map
		    		 */
					google.maps.event.addDomListener( window, 'load', initialize_map() );
					google.maps.event.addDomListener(window, 'resize', function() {
					    initialize_map();
					} );
		    	} else {
		    		locations_result += '<option value="No Location Found" data-id=""></option>';
		    	}

		    	/**
		    	 * Place the constructed HTML
		    	 */
		    	$( '#b-locations' ).html( locations_result );

		    	/**
		    	 * Input in search field
		    	 */
		    	var inputed, location_id;
		    	$( '#b-search' ).on( 'input', function() {
		    		inputed = $( 'option[value="'+ $( this ).val() +'"]' );
		    		location_id = inputed.length ? inputed.data( 'id' ) : '';

		    		if( location_id != '' && location_id != undefined )
		    			$( this ).attr( 'data-locationid', location_id );	
		    	} );
		    	
		    	get_locations_by_categories();
		    	$( '.search-field .search-btn' ).on( 'click', function( e ) {
					e.preventDefault();

			    	if( location_id != '' && location_id != undefined ) {
				    	get_locations_by_categories();
			    	}
				} );
		    },
		    error    : function( response ) {
		    	$( '#b-locations' ).html( '<option value="Loading Failed" data-id=""></option>' );

		    	console.log( response );
		    }
		} );
	}

	/**
	 * Get the location by the selected categories
	 */
	function get_locations_by_categories()
	{
		var category_id = $( '.search-field #b-search' ).attr( 'data-locationid' );
		
		$( '.b-preloader' ).show();
		$.ajax( {
		    url      : ajaxurl,
		    type     : 'post',
		    dataType : 'json',
		    data     : {
		        action      : 'get_locations_by_categories',
		        category_id : category_id
		    },
		    success  : function( response ) {
		    	$( '.b-preloader' ).hide();
		    	console.log( response );
		    	/**
		    	 * Initialize the map and place all the location
		    	 */
		    	map_settings = response.map_settings

		    	var locations_result = '';

		    	locations_data = {};

		    	if ( response.filtered_locations ) {
		    		/**
		    		 * Get the locations data
		    		 */
		    		$.each( response.filtered_locations, function( index, val ) {
		    			/* Store the location data in the array that can be used in initializing map */
		    			locations_data[ val.location_id ] = [
							val.location_long,
							val.location_lat,
							val.location_details,
						];
		    		} );
		    		
		    		/**
		    		 * Initialize the map
		    		 */
					google.maps.event.addDomListener( window, 'load', initialize_map() );
					google.maps.event.addDomListener(window, 'resize', function() {
					    initialize_map();
					} );

					/**
					 * Create list of location filtered by selected category
					 */
					locations_result += '<div class="location-result">';
						locations_result += '<div class="location-result-search-field">';
							locations_result += '<input type="text" name="search-location" placeholder="Search location" />';
							locations_result += '<i class="fa fa-search" aria-hidden="true"></i>';
						locations_result += '</div>';
						locations_result += '<div class="location-result-list">';
							$.each( response.filtered_locations, function( index, val ) {
				    			locations_result += '<div class="location-result-list-item transition" data-resultid="'+ val.location_id +'">';
				    				locations_result += '<h2 class="location-title">'+ val.location_name +'</h2>';

				    				if( val.location_list_address.length )
					    				locations_result += '<p class="location-address"><i class="fa fa-map-marker" aria-hidden="true"></i>'+ val.location_list_address +'</p>';

					    			if( val.location_list_contact_number.length )
					    				locations_result += '<p class="location-contact-number"><i class="fa fa-phone" aria-hidden="true"></i>'+ val.location_list_contact_number +'</p>';

					    			if( val.location_list_schedule.length )
					    				locations_result += '<p class="location-schedule">'+ val.location_list_schedule +'</p>';
				    			locations_result += '</div>';
				    		} );
						locations_result += '</div>';
					locations_result += '</div>';

					$( '.list-location' ).addClass( 'location-result-container' );
					$( '.list-location .content' ).html( locations_result );

					/**
					 * Initialize nicescroll
					 */
					var args = {
						cursorwidth        : 4,
				        zindex             : 9999999,
				        horizrailenabled   : false,
				        cursorborderradius : 3,
				        background         : "transparent",
				        cursorcolor        : '#f8bf00',
				        cursorborder       : '1px solid #f8bf00',
				        autohidemode	   : false
					};
					$( '.location-result-list' ).niceScroll( args );

					if ( response.filtered_locations.length <= 2 ) {
						$( '.location-result-list' )
							.getNiceScroll()
							.hide();
					}
					else {
						$( '.location-result-list' )
							.addClass( 'padd-r-10' )
							.getNiceScroll()
							.show();
					}

					$( 'html, body' ).getNiceScroll().resize();

					/**
					 * Search location in result list
					 */
					$( 'input[name="search-location"]' ).keyup( function( event ) {
						var search = $( this ).val().toUpperCase();
						var counter_search = 0;
						$( '.location-result-list-item' ).each( function( index, el ) {
							if( $( 'h2', el ).text().toUpperCase().indexOf( search ) > -1 ) {
								$( el ).show();
								counter_search++;
							} else {
								$( el ).hide();
							}
						} );

						if ( counter_search <= 2 ) {
							$( '.location-result-list' )
								.css( 'padding-right', '0' )
								.getNiceScroll()
								.hide();
						}
						else {
							$( '.location-result-list' )
								.css( 'padding-right', '10px' )
								.getNiceScroll()
								.show();
						}
					} );

		    	} else {
		    		if ( category_id != '' ) {
			    		swal(
							'Search Again',
							'No location found',
							'warning'
						).then( swal.noop );
		    		}
		    	}
		    },
		    error    : function( response ) {
		    	$( '#b-locations' ).html( '<option value="Loading Failed" data-id=""></option>' );

		    	console.log( response );
		    }
		} );
	}

	/**
	 * Initialize map show the location
	 */
	function initialize_map()
	{
		var settings = {
			zoom                 	: parseInt( zoom_level ),
			center               	: new google.maps.LatLng( center_long, center_lat, 16 ),
			mapTypeId            	: google.maps.MapTypeId.ROADMAP,
			draggable  				: ( google_map_draggable == 'on' ) ? false : true,
			disableDoubleClickZoom  : ( google_map_doubleclickzoom == 'on' ) ? false : true,
			zoomControl  			: ( google_map_zoomcontrol == 'on' ) ? false : true,
			scrollwheel 			: ( google_map_scrollwheel == 'on' ) ? false : true,
			streetViewControl		: ( google_map_streetview == 'on' ) ? false : true,
		};

		if ( google_map_theme != '' )
			settings['styles'] = eval( google_map_theme );
		
		// console.log( JSON.parse( google_map_theme ) );
		var map = new google.maps.Map( document.getElementById( 'b-map' ), settings );

		var infowindow = new google.maps.InfoWindow();

		var marker;
		
		/* Initialize the action when clicked the marker in google map */
		for ( var key in locations_data ) {
			/* Populate the map */
			var data = {
				position	: new google.maps.LatLng(locations_data[key][0], locations_data[key][1]),
				map     	: map,
			};
			if ( google_map_marker != '' )
				data['icon'] = google_map_marker;

		    marker = new google.maps.Marker( data );

		    /* Action in clicking the pin in the map */
			google.maps.event.addListener( marker, 'click', ( function( marker, key ) {
				return function() {
					/* Show details of the location */
					infowindow.setContent( locations_data[key][2] );
					infowindow.open( map, marker );
				}
			} ) ( marker, key ) );
		}

		/* Action in clicking search button */
		$( document ).on( 'click', '.location-result-list-item', function( event ) {
			$( '.location-result-list-item' ).removeClass( 'active-list-item' );
			$( this ).addClass( 'active-list-item' );

			var id = $( this ).attr( 'data-resultid' );
			if ( id != undefined && id != '' ) {
				var data = {
					position	: new google.maps.LatLng(locations_data[id][0], locations_data[id][1]),
					map     	: map,
				};
				if ( google_map_marker != '' )
					data['icon'] = google_map_marker;
				marker = new google.maps.Marker( data );

				/* Show details of the location */
				infowindow.setContent( locations_data[id][2] );
				infowindow.open( map, marker );

				/* Initialize the action when clicked the marker in google map */
				for ( var key in locations_data ) {
					/* Populate the map */
				    var data = {
						position	: new google.maps.LatLng(locations_data[key][0], locations_data[key][1]),
						map     	: map,
					};
					if ( google_map_marker != '' )
						data['icon'] = google_map_marker;

				    marker = new google.maps.Marker( data );

				    /* Action in clicking the pin in the map */
					google.maps.event.addListener( marker, 'click', ( function( marker, key ) {
						return function() {
							/* Show details of the location */
							infowindow.setContent( locations_data[key][2] );
							infowindow.open( map, marker );
						}
					} ) ( marker, key ) );
				}
			}
		} );
	}
} ( jQuery ) );