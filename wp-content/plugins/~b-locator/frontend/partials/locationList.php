<div id="b-location-list" class="b-container <?php echo ( $categories ) ? 'have-categories' : '' ?> <?php echo ( $search ) ? 'have-search' : '' ?> <?php echo ( $locations ) ? 'have-location' : '' ?>">
	<?php if ( $categories ): ?>
		<select name="b-categories" id="b-categories">
			<option>- Select Categories -</option>
			<?php foreach ( $categories as $category ): ?>
				<option value="<?php echo $category->slug ?>"><?php echo $category->name ?></option>
			<?php endforeach ?>
		</select>
	<?php endif ?>

	<?php if ( $search && $locations ): ?>
		<div class="search-field">
			<div class="b-preloader"></div>
			
			<input type="text" name="b-search" id="b-search" list="b-locations" placeholder="Search for any Andoks Branch" data-locationid=<?php echo ( isset( $_GET['locationid'] ) ) ? $_GET['locationid'] : '' ?>>
			<div class="search-btn"></div>
		</div>

		<datalist id="b-locations">
			<option value="Loading Locations.." data-id=""></option>
		</datalist>
	<?php endif ?>
</div>

<?php wp_reset_postdata() ?>